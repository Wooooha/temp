var IS_CHROME = !!window.webkitRTCPeerConnection,
	RTCPeerConnection,
	RTCIceCandidate,
	RTCSessionDescription;

if (IS_CHROME) {
	RTCPeerConnection = webkitRTCPeerConnection;
	RTCIceCandidate = window.RTCIceCandidate;
	RTCSessionDescription = window.RTCSessionDescription;
} else {
	RTCPeerConnection = mozRTCPeerConnection;
	RTCIceCandidate = mozRTCIceCandidate;
	RTCSessionDescription = mozRTCSessionDescription;
}

class SignalingChannel {
	constructor (peerConnection) {
		this.peerConnection = peerConnection;
	}

	send (message) {
		var data = JSON.stringify(message);
		console.log(data);
		// Send messages using your favorite real-time network
	}

	onmessage (message) {
		var data = JSON.parse(message);
		console.log(data);

		// If we get a sdp we have to sign and return it
		if (message.sdp != null) {
			var that = this;
			this.peerConnection.setRemoteDescription(new RTCSessionDescription(message.sdp), function () {
				that.peerConnection.createAnswer(function (description) {
					that.send(description);
				});
			});
		} else {
			this.peerConnection.addIceCandidate(new RTCIceCandidate(message.candidate));
		}
	}
}

// Create a peer connection object
var connection = new RTCPeerConnection({
	iceServers: [
		{
			urls: 'stun:12.345.678.910:3478',
		},
		{
			urls: 'turn:12.345.678.910:3479',
			username: 'ninefingers',
			credential: 'youhavetoberealistic',
		},
	],
});

// Initiate a signaling channel between two users
var signalingChannel = new SignalingChannel(connection);

// Only one client should initiate the connection, the other client should wait.
function initiateConnection () {
	connection.createOffer(function (description) {
		signalingChannel.send(description);
	});
}

// Firefox does not support unreliable channels at this time
var dataChannel = connection.createDataChannel('my_label', IS_CHROME ? { reliable: false } : {});

dataChannel.onmessage = function (event) {
	var data = event.data;

	console.log('I got data channel message: ', data);
};

dataChannel.onopen = function () {
	dataChannel.send('Hello World!');
};

export default initiateConnection;
