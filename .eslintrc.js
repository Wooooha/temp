module.exports = {
	root: true,
	extends: ['eslint:recommended', 'plugin:react/recommended', 'plugin:flowtype/recommended'],
	plugins: ['react', 'flowtype'],
	parserOptions: {
		ecmaVersion: 8,
		sourceType: 'module',
		ecmaFeatures: {
			jsx: true,
		},
	},
	settings: {
		react: {
			createClass: 'createReactClass',
			pragma: 'React',
			version: '^16.4.2',
			flowVersion: '0.53',
		},
	},
	rules: {
		'flowtype/define-flow-type': 1,
		'flowtype/generic-spacing': [2, 'never'],
		'flowtype/no-types-missing-file-annotation': 0,

		// allow async-await
		'generator-star-spacing': 'off',
		indent: [2, 'tab'],
		'max-len': [0, 80, 4],
		'no-mixed-spaces-and-tabs': 0,
		'no-sparse-arrays': 2,
		'no-undef': 'off',
		'no-tabs': 'off',
		'no-trailing-spaces': 2,
		'no-multiple-empty-lines': [2, { max: 2 }],
		'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		semi: [2, "always", { "omitLastInOneLineBlock": true}],
		'space-before-function-paren': 1,
		'spaced-comment': 2,
		'space-in-parens': [0, 'never'],
		'keyword-spacing': 2,
		'no-console': 0,
	},
};
