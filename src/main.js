import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './main.less';

type Props = {
	name: number,
};

type State = {
	year: number,
};

class HelloMessage extends Component<Props, State> {
	props: Props;
	state: State;

	state = {
		year: 686868,
	};

	constructor (props: Props) {
		super(props);
	}

	componentDidMount () {
		removeLoader();
	}

	render () {
		return (
			<div>
				<span>Hello {this.props.name}</span>
				<button id="initiate-button" onClick={() => {}}>
					邀请
				</button>
			</div>
		);
	}
}

ReactDOM.render(<HelloMessage name="Eilfer" />, document.getElementById('content'));

function removeLoader () {
	document.querySelector('.loader').classList.add('hide');
}

if (module.hot) {
	module.hot.dispose(function () {
		// 模块即将被替换时
	});

	module.hot.accept(function () {
		// 模块或其依赖项之一刚刚更新时
	});
}
